# deep_learning_capillary_analysis_paper

Code necessary to produce results and figure for our paper describing the new deep learning based approach to detecting and measuring capillaries in nailfold images, and subsequently using these measures to predict subject group status. 